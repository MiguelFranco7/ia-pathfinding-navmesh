#include "stdafx.h"
#include "AlignToMovementSteering.h"
#include "character.h"

AlignToMovementSteering::AlignToMovementSteering(Character *character) {
	mCharacter	   = character;
}

float AlignToMovementSteering::GetSteering() {
	// Calculo angulo de direccion
	USVec2D targetPosition = mCharacter->GetLoc() + mCharacter->GetLinearVelocity();
	float angle = atan2(targetPosition.mY - mCharacter->GetLoc().mY, targetPosition.mX - mCharacter->GetLoc().mX) * 180 / PI;

	mSteering = AlignSteering(mCharacter, angle);
	return mSteering.GetSteering();
}

void AlignToMovementSteering::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 0.5f);
}
