#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include <moaicore/MOAIEntity2D.h>
#include <params.h>
#include "SeekSteering.h"
#include "ArriveSteering.h"
#include "AlignSteering.h"
#include "AlignToMovementSteering.h"
#include "PursueSteering.h"
#include "PathFollowingSteering.h"
#include "ObstacleAvoidanceSteering.h"

class Character: public MOAIEntity2D {
public:
    DECL_LUA_FACTORY(Character)
protected:
	virtual void OnStart();
	virtual void OnStop();
	virtual void OnUpdate(float step);
public:
	virtual void DrawDebug();

	Character();
	~Character();
	
	void SetLinearVelocity(float x, float y) { mLinearVelocity.mX = x; mLinearVelocity.mY = y;}
	void SetAngularVelocity(float angle) { mAngularVelocity = angle;}
	
	USVec2D			  GetLinearVelocity() const { return mLinearVelocity;}
	float			  GetAngularVelocity() const { return mAngularVelocity;}
	Params			  GetParams() const { return mParams; }
	USVec2D			  GetTarget() { return mTarget; }
	void			  SetTarget(USVec2D target) { mTarget = target; }
	vector<Obstacles> GetObstacles() { return mObstacles; }
	void              SetObstacleCollision(int pos, bool collision) { mObstacles[pos].collision = collision; }

	void UpdatePath(vector<USVec2D> pathfollowing);
protected:
	USVec2D mLinearVelocity;
	float mAngularVelocity;
	
	Params					  mParams;
	vector<USVec2D>			  mPath;
	vector<Obstacles>		  mObstacles;
	USVec2D					  mTarget;
	SeekSteering			  seek;
	ArriveSteering			  arrive;
	AlignSteering			  align;
	AlignToMovementSteering   alignToMovement;
	PursueSteering			  pursue;
	PathFollowingSteering	  pathFollowing;
	ObstacleAvoidanceSteering obstacleAvoidance;

	// Lua configuration
public:
	virtual void RegisterLuaFuncs(MOAILuaState& state);
private:
	static int _setLinearVel(lua_State* L);
	static int _setAngularVel(lua_State* L);
	static int _setPathfinder(lua_State* L);
};

#endif
