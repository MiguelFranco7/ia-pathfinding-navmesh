#include <stdafx.h>
#include <tinyxml.h>
#include "params.h"

bool ReadParams(const char* filename, Params& params) {
	TiXmlDocument doc(filename);
	if (!doc.LoadFile())
	{
		fprintf(stderr, "Couldn't read params from %s", filename);
		return false;
	}

	TiXmlHandle hDoc(&doc);

	TiXmlElement* pElem;
	pElem = hDoc.FirstChildElement().Element();
	if (!pElem)
	{
		fprintf(stderr, "Invalid format for %s", filename);
		return false;
	}

	TiXmlHandle hRoot(pElem);
	TiXmlHandle hParams = hRoot.FirstChildElement("params");

	//TiXmlElement* paramElem = hParams.FirstChild().Element();
	//for (paramElem; paramElem; paramElem = paramElem->NextSiblingElement())
	//{
	//    const char* paramName = paramElem->Value();
	//    if (!strcmp(paramName, "max_velocity"))
	//    {
	//        const char* elemText = paramElem->GetText();
	//        params.max_velocity = static_cast<float>(atof(elemText));
	//    }
	//}

	TiXmlElement* paramElem = hParams.FirstChildElement("max_velocity").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.max_velocity);

	paramElem = hParams.FirstChildElement("max_acceleration").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.max_acceleration);

	paramElem = hParams.FirstChildElement("dest_radius").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.dest_radius);

	paramElem = hParams.FirstChildElement("arrive_radius").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.arrive_radius);

	paramElem = hParams.FirstChildElement("targetPosition").Element();
	if (paramElem)
	{
		paramElem->Attribute("x", &params.targetPosition.mX);
		paramElem->Attribute("y", &params.targetPosition.mY);
	}

	paramElem = hParams.FirstChildElement("max_angular_velocity").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.max_angular_velocity);

	paramElem = hParams.FirstChildElement("max_angular_acceleration").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.max_angular_acceleration);

	paramElem = hParams.FirstChildElement("angular_dest_radius").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.angular_dest_radius);

	paramElem = hParams.FirstChildElement("angular_arrive_radius").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.angular_arrive_radius);

	paramElem = hParams.FirstChildElement("targetRotation").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.targetRotation);

	paramElem = hParams.FirstChildElement("char_radius").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.charRadius);

	paramElem = hParams.FirstChildElement("look_ahead").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.lookAhead);

	paramElem = hParams.FirstChildElement("time_ahead").Element();
	if (paramElem)
		paramElem->Attribute("value", &params.timeAhead);

	return true;
}

bool ReadPath(const char* filename, vector<USVec2D> &path) {
	TiXmlDocument doc(filename);
	if (!doc.LoadFile()) {
		fprintf(stderr, "Couldn't read params from %s", filename);
		return false;
	}

	TiXmlHandle hDoc(&doc);

	TiXmlElement* pElem;
	pElem = hDoc.FirstChildElement().Element();
	if (!pElem) {
		fprintf(stderr, "Invalid format for %s", filename);
		return false;
	}

	TiXmlHandle hRoot(pElem);
	TiXmlHandle hParams = hRoot.FirstChildElement("points");

	USVec2D pointTemp;
	TiXmlElement* paramElem = hParams.FirstChild("point").Element();
	for (paramElem; paramElem; paramElem = paramElem->NextSiblingElement()) {
		const char* paramName = paramElem->Value();
		if (!strcmp(paramName, "point")) {
			paramElem->Attribute("x", &pointTemp.mX);
			paramElem->Attribute("y", &pointTemp.mY);

			path.push_back(pointTemp);
		}
	}

	return true;
}

bool ReadNav(const char* filename, vector<NavPolygon> &navMesh) {
	TiXmlDocument doc(filename);
	if (!doc.LoadFile()) {
		fprintf(stderr, "Couldn't read params from %s", filename);
		return false;
	}

	TiXmlHandle hDoc(&doc);

	TiXmlElement* pElem;
	pElem = hDoc.FirstChildElement().Element();
	if (!pElem) {
		fprintf(stderr, "Invalid format for %s", filename);
		return false;
	}

	NavPolygon		 navPolTemp;
	USVec2D			 pointTemp;
	NavPolygon::Edge edgeTemp;
	int vert = 0;
	int id = 0;

	TiXmlHandle hRoot(pElem);

	// Rellenar poligonos
	TiXmlHandle hPolygons = hRoot.FirstChildElement("polygons");

	TiXmlElement* polygonElem = hPolygons.FirstChildElement("polygon").Element();
	for (polygonElem; polygonElem; polygonElem = polygonElem->NextSiblingElement()) {
		TiXmlElement* pointElem = polygonElem->FirstChildElement("point");
		for (pointElem; pointElem; pointElem = pointElem->NextSiblingElement()) {
			const char* paramName = pointElem->Value();
			if (!strcmp(paramName, "point")) {
				pointElem->Attribute("x", &pointTemp.mX);
				pointElem->Attribute("y", &pointTemp.mY);
				edgeTemp.mPNeighbour = nullptr;
				edgeTemp.mVerts[0] = vert;
				if (pointElem->NextSiblingElement() == NULL)
					edgeTemp.mVerts[1] = 0;
				else
					edgeTemp.mVerts[1] = vert + 1;
				

				navPolTemp.mVerts.push_back(pointTemp);
				navPolTemp.mEdges.push_back(edgeTemp);
				vert++;
			}			
		}

		navPolTemp.actualCost = 0;
		navPolTemp.totalCost = 0;
		navPolTemp.cost		 = 1;
		navPolTemp.father	 = nullptr;
		navPolTemp.isOpen = false;
		navPolTemp.isClose = false;
		navPolTemp._id = id;
	    
		navMesh.push_back(navPolTemp);
		navPolTemp.mVerts.clear();
		navPolTemp.mEdges.clear();
		vert = 0;
		id++;
	}

	// Rellenar links
	TiXmlHandle hLinks = hRoot.FirstChildElement("links");
	int polygon1;
	int polygon2;
	int edge1;
	int edge2;

	TiXmlElement* linkElem = hLinks.FirstChildElement("link").Element();
	for (linkElem; linkElem; linkElem = linkElem->NextSiblingElement()) {
		TiXmlElement* startElem = linkElem->FirstChildElement("start");
		TiXmlElement* endElem   = linkElem->FirstChildElement("end");
		const char* startName = startElem->Value();
		const char* endName = endElem->Value();
		if (!strcmp(startName, "start") && !strcmp(endName, "end")) {
			startElem->Attribute("polygon", &polygon1);
			startElem->Attribute("edgestart", &edge1);
			endElem->Attribute("polygon", &polygon2);
			endElem->Attribute("edgestart", &edge2);

			navMesh[polygon1].mEdges[edge1].mPNeighbour = &navMesh[polygon2];
			navMesh[polygon2].mEdges[edge2].mPNeighbour = &navMesh[polygon1];
		}
	}

	return true;
}

bool ReadObstacles(const char * filename, vector<Obstacles>& obstacles) {
	TiXmlDocument doc(filename);
	if (!doc.LoadFile()) {
		fprintf(stderr, "Couldn't read params from %s", filename);
		return false;
	}

	TiXmlHandle hDoc(&doc);

	TiXmlElement* pElem;
	pElem = hDoc.FirstChildElement().Element();
	if (!pElem) {
		fprintf(stderr, "Invalid format for %s", filename);
		return false;
	}

	TiXmlHandle hRoot(pElem);
	TiXmlHandle hParams = hRoot.FirstChildElement("obstacles");

	Obstacles obstacleTemp;
	TiXmlElement* paramElem = hParams.FirstChild("obstacle").Element();
	for (paramElem; paramElem; paramElem = paramElem->NextSiblingElement()) {
		const char* paramName = paramElem->Value();
		if (!strcmp(paramName, "obstacle")) {
			paramElem->Attribute("x", &obstacleTemp.position.mX);
			paramElem->Attribute("y", &obstacleTemp.position.mY);
			paramElem->Attribute("r", &obstacleTemp.radius);
			obstacleTemp.collision = false;

			obstacles.push_back(obstacleTemp);
		}
	}

	return true;
}
