#pragma once

#include "uslscore\USVec2D.h"
#include "ArriveSteering.h"

class Character;

class PathFollowingSteering {
public:
	PathFollowingSteering() {}
	PathFollowingSteering(Character *character, vector<USVec2D> path);
	USVec2D GetSteering();
	void	DrawDebug();

	USVec2D GetNextPoint() { return mNextPoint; }

	Character	  *mCharacter;
	USVec2D		   mAcce;
	USVec2D		   mDreamVelocity;
	ArriveSteering mArrive;

private:
	int				mCurrentNode;
	vector<USVec2D> mPath;
	USVec2D			mNearPoint;
	USVec2D			mNextPoint;

	USVec2D FindNearPointToSegment();
};
